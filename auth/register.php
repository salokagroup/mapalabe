<?php

  require_once '../config/db.php';

  header("Access-Control-Allow-Origin: * ");
  header("Content-Type: application/json; charset=UTF-8");
  header("Access-Control-Allow-Methods: POST");
  header("Access-Control-Max-Age: 3600");
  header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
      // get posted data
      $data = json_decode(file_get_contents("php://input", true));
      
      $sql = "INSERT INTO user (nama, username, password) VALUES('" . mysqli_real_escape_string($dbConn, $data->nama) . "', '" . mysqli_real_escape_string($dbConn, $data->username) . "', '" . mysqli_real_escape_string($dbConn, $data->password) . "')";
      
      $result = dbQuery($sql);
      
      if($result) {
          echo json_encode(array('success' => 'You registered successfully'));
      } else {
          echo json_encode(array('error' => 'Something went wrong, please contact administrator'));
      }
  }