<?php

    require_once '../config/db.php';
    require_once '../class/jwt-utils.php';

    header("Access-Control-Allow-Origin: * ");
    header("Content-Type: application/json; charset=UTF-8");
    header("Access-Control-Allow-Methods: POST");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        // get posted data
        $data = json_decode(file_get_contents("php://input", true));
        
        $sql = "SELECT * FROM user WHERE username = '" . mysqli_real_escape_string($dbConn, $data->username) . "' AND password = '" . mysqli_real_escape_string($dbConn, $data->password) . "' LIMIT 1";
        var_dump($sql);
        $result = dbQuery($sql);
        
        if(dbNumRows($result) < 1) {
            http_response_code(400);
            echo json_encode(array('error' => 'Invalid User'));
        } else {
            http_response_code(200);
            $row = dbFetchAssoc($result);
            
            $username = $row['username'];
            
            $headers = array('alg'=>'HS256','typ'=>'JWT');
            $payload = array('username'=>$username, 'exp'=>(time() + 60));

            $jwt = generate_jwt($headers, $payload);
            
            echo json_encode(array('token' => $jwt));
        }
    }