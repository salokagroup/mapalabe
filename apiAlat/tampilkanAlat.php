<?php
    
    include_once '../config/database.php';
    include_once '../class/alat.php';

    $database = new Database();
    $db = $database->getConnection();

    $items = new Alat($db);

    $stmt = $items->getAlat();
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        
        $barangArr = array();
        $barangArr["body"] = array();
        $barangArr["itemCount"] = $itemCount;

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id_alat,
                "nama" => $nama,
                "jumlah" => $jumlah,
                "kategori" => $kategori,
                "deskripsi" => $deskripsi
            );

            array_push($barangArr["body"], $e);
        }
        echo json_encode($barangArr);
    }

    else{
        http_response_code(404);
        echo json_encode(
            array("message" => "No record found.")
        );
    }
?>
