<?php
    class Divisi{

        // Connection
        private $conn;

        // Table
        private $db_table = "divisi";

        // Columns
        public $id_divisi;
        public $nama_divisi;

        // Db connection
        public function __construct($db){
            $this->conn = $db;
        }

        // GET ALL
        public function getDivisi(){
            $sqlQuery = "SELECT id_divisi, nama_divisi FROM " . $this->db_table . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE
        public function createDivisi(){
            $sqlQuery = "INSERT INTO
                        ". $this->db_table ."
                    SET
                        nama_divisi = :nama_divisi";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->nama_divisi=htmlspecialchars(strip_tags($this->nama_divisi));
        
            // bind data
            $stmt->bindParam(":nama_divisi", $this->nama_divisi);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // READ single
        public function getSingleEmployee(){
            $sqlQuery = "SELECT
                        id, 
                        name, 
                        email, 
                        age, 
                        designation, 
                        created
                      FROM
                        ". $this->db_table ."
                    WHERE 
                       id = ?
                    LIMIT 0,1";

            $stmt = $this->conn->prepare($sqlQuery);

            $stmt->bindParam(1, $this->id);

            $stmt->execute();

            $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
            
            $this->name = $dataRow['name'];
            $this->email = $dataRow['email'];
            $this->age = $dataRow['age'];
            $this->designation = $dataRow['designation'];
            $this->created = $dataRow['created'];
        }        

        // UPDATE
        public function updateDivisi(){
            $sqlQuery = "UPDATE
                        ". $this->db_table ."
                    SET
                        nama_divisi = :nama_divisi
                    WHERE 
                        id_divisi = :id_divisi";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->nama_divisi=htmlspecialchars(strip_tags($this->nama_divisi));
            $this->id_divisi=htmlspecialchars(strip_tags($this->id_divisi));
        
            // bind data
            $stmt->bindParam(":nama_divisi", $this->nama_divisi);
            $stmt->bindParam(":id_divisi", $this->id_divisi);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE
        function deleteDivisi(){
            $sqlQuery = "DELETE FROM " . $this->db_table . " WHERE id_divisi = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id_divisi=htmlspecialchars(strip_tags($this->id_divisi));
        
            $stmt->bindParam(1, $this->id_divisi);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>